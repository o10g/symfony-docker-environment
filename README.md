### What is this repository for? ###

Setup the symfony environment with docker for development

### How to setup it ###


```
docker build -t symfony/nginx sfnginx
docker build -t symfony/php-fpm php-fpm
docker build -t symfony/code code
docker pull sameersbn/mysql
docker-compose up -d
```